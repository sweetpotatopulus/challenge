"""
Model file.

@author: maldonadoraulqro@gmail.com
"""
import json
import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.text import tokenizer_from_json
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Dense, Embedding, Concatenate
from tensorflow.keras.layers import Input, Lambda  # Conv1D, GlobalMaxPooling1D
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import Model
from tensorflow.keras.initializers import Constant

BASE_DIR = ('./utils/' if os.path.isfile('./utils/words.npy')
            else './challenge/utils/')
PATH_TO_EMBEDING = os.path.join(BASE_DIR, 'embeding_matrix.npy')
PATH_TO_WEIGHTS = os.path.join(BASE_DIR, 'model_weights.hdf5')
PATH_TO_GENRES_DICT = os.path.join(BASE_DIR, 'genres.txt')
PATH_TO_LANGS = os.path.join(BASE_DIR, 'langs.npy')
TOKENIZER_PATH = os.path.join(BASE_DIR, 'tokenizerwo.txt')
WORDS_PATH = os.path.join(BASE_DIR, 'words.npy')
np.random.seed(0)


def format_2_string(data):
    """Convert a dataframe to a printable string."""
    header = ','.join(data.columns) + '\n'
    as_string = data.astype(str).apply(','.join, axis=1).to_string(index=False)
    return header + as_string


def sort_and_get_genre(y_train):
    """Convert predictions into Top 5 Sorted Genres."""
    with open(PATH_TO_GENRES_DICT) as gen_file:
        genres_dict = json.load(gen_file)
    short_n_sort = np.argsort(-y_train)[:, :5]
    gen_lst = [' '.join([genres_dict[str(key)] for key in movie_ix])
               for movie_ix in short_n_sort]
    return gen_lst


def clean_data(data):
    """Clean data."""
    synps_len = data.synopsis.map(len)
    data = data[synps_len > int(synps_len.quantile(.05))]
    lang_list = np.load(PATH_TO_LANGS)
    data = data.loc[map(lambda x: bool(x == 'en'), lang_list)]
    labels = data.genres.str.get_dummies(sep=' ').values
    return data, labels


def generate_callbacks():
    """Generate callbacks."""
    checkpointer = ModelCheckpoint(monitor='val_precision', mode='max',
                                   filepath=PATH_TO_WEIGHTS,
                                   verbose=1, save_best_only=True,
                                   save_weights_only=True)
    reduce_lr = ReduceLROnPlateau(verbose=1, factor=0.25, patience=3,
                                  min_lr=0.0000001)
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0.001,
                               patience=15, restore_best_weights=True)
    return [checkpointer, reduce_lr, early_stop]


def calculate_interesting_words(data, sequences):
    """Calculate interesting words."""
    words_of_interes = np.load(WORDS_PATH)
    for word in words_of_interes:
        data[word] = data.synopsis.map(lambda x: int(word in x))
    return np.concatenate((sequences, data[words_of_interes].values), axis=1)


def crop():
    """Crop tensor with lambda layer."""
    def func(tensor):
        return tensor[:, :800], tensor[:, 800:]
    return Lambda(func)


class Mymodel():
    """My model."""

    def __init__(self):
        self.max_num_words = 20000
        self.max_sequence_lenght = 250
        self.val_test_split = .2
        self.embeding_dim = 100
        with open(TOKENIZER_PATH, 'r') as myfile:
            red_file = myfile.read()
        token_json = json.loads(red_file)
        self.tokenizer = tokenizer_from_json(token_json)
        self.model = self.build_model()

    def build_model(self):
        """Create the model."""
        embeding_matrix = np.load(PATH_TO_EMBEDING)
        embedding_layer = Embedding(self.max_num_words,
                                    self.embeding_dim,
                                    embeddings_initializer=Constant(
                                        embeding_matrix),
                                    input_length=self.max_sequence_lenght,
                                    trainable=False)
        sequence_input = Input(
            shape=(self.max_sequence_lenght + 38,), dtype='int32')
        x_synps, top_words = crop()(sequence_input)
        embedded_sequences = embedding_layer(x_synps)
        x_tentacion = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(32))(embedded_sequences)
        x_tentacion = tf.keras.layers.Dense(64, activation='relu')(x_tentacion)
        x_tentacion = tf.keras.layers.Dense(32, activation='relu')(x_tentacion)
        x_tentacion = Concatenate(
            axis=-1)([x_tentacion, tf.cast(top_words, 'float32')])
        x_tentacion = Dense(200, activation='relu')(x_tentacion)
        x_tentacion = Dense(200, activation='relu')(x_tentacion)
        preds = Dense(19, activation='sigmoid')(x_tentacion)
        model = Model(sequence_input, preds)
        model.compile(loss='binary_crossentropy', optimizer='adam',
                      metrics=['accuracy',
                               tf.keras.metrics.Precision(top_k=5)])
        return model

    def preprocessing(self, data):
        """Process the data."""
        synpses = data.synopsis.values
        sequences = self.tokenizer.texts_to_sequences(synpses)
        padded_seq = pad_sequences(sequences, maxlen=self.max_sequence_lenght)
        return calculate_interesting_words(data, padded_seq)

    def validation_split(self, data, labels):
        """Split the data in train and validation."""
        indices = np.arange(data.shape[0])
        np.random.shuffle(indices)
        data = data[indices]
        labels = labels[indices]
        num_validation_samples = int(self.val_test_split * data.shape[0])

        x_train = data[:-num_validation_samples]
        y_train = labels[:-num_validation_samples]
        x_val = data[-num_validation_samples:]
        y_val = labels[-num_validation_samples:]
        return [tf.convert_to_tensor(x_set, dtype='float32')
                for x_set in [x_train, y_train, x_val, y_val]]

    def train(self, data):
        """Train the model."""
        cleansed_data, labels = clean_data(data)
        processed_data = self.preprocessing(cleansed_data)
        x_train, y_train, x_val, y_val = self.validation_split(processed_data,
                                                               labels)
        self.model.fit(x_train, y_train, batch_size=32, epochs=30,
                       validation_data=(x_val, y_val),
                       callbacks=generate_callbacks())

    def predict(self, data):
        """Predict data."""
        processed_data = self.preprocessing(data)
        self.model.load_weights(PATH_TO_WEIGHTS)
        predictions = self.model.predict(processed_data)
        submission = data[['movie_id']].copy()
        submission.loc[:, 'predicted_genres'] = sort_and_get_genre(predictions)
        return format_2_string(submission)
