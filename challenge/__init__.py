"""
Challenge API __init__.

@author: raul
"""
from .challenge import app


__all__ = ['app']
