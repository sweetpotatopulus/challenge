"""
Challenge API file.

@author: maldonadoraulqro@gmail.com
"""
import io
import sys
import pandas as pd
from flask import Flask, request, jsonify
sys.path.insert(0, './challenge/')
from mymodel import Mymodel


app = Flask(__name__)
MODEL = Mymodel()


@app.route('/')
def hello_world():
    """Say hello."""
    return "Hello world!"


@app.route("/genres/train", methods=['POST'])
def train_model():
    """Train the model."""
    stream = io.StringIO(request.get_data().decode("UTF8"), newline=None)
    data = pd.read_csv(stream)
    MODEL.train(data)
    return jsonify({"status": "Model Correctly saved at /utils/weights.hdf5."})


@app.route("/genres/predict", methods=['POST'])
def test_model():
    """Test the model."""
    stream = io.StringIO(request.get_data().decode("UTF8"), newline=None)
    data = pd.read_csv(stream)
    predictions = MODEL.predict(data)
    return predictions
