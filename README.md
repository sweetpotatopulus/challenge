# Radix.ai Machine Learning Challenge

## Introduction

The goal of this challenge is to build a Machine Learning model to predict the genres of a movie, given its synopsis. Your solution will be evaluated on the performance of your Machine Learning model and on the quality of your code.

To succeed, you must implement a Python package called `challenge`, which exposes a Flask REST API according to the REST API specification in `api.yml` (you can view this specification at [editor.swagger.io](https://editor.swagger.io)).

Specifically, the Flask REST API exposes:

1. A training endpoint at `localhost:5000/genres/train` to which you can POST a CSV with header `movie_id,synopsis,genres`, where `genres` is a space-separated list of movie genres.
2. A prediction endpoint at `localhost:5000/genres/predict` to which you can POST a CSV with header `movie_id,synopsis` and returns a CSV with header `movie_id,predicted_genres`, where `predicted_genres` is a space-separated list of the top 5 movie genres (sorted).